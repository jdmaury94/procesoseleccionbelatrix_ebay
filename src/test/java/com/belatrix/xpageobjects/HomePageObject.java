package com.belatrix.xpageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class HomePageObject extends PageObject {
	
	@FindBy(id="gh-btn")
	WebElementFacade btnSearch;
	
	@FindBy(id="gh-ac")
	WebElementFacade salesPerson;
	
	public void clickSearchBtn() {
		btnSearch.click();
	}
	
	public void inputTextSalesPerson(String palabraBusqueda) {
		salesPerson.sendKeys(palabraBusqueda);
	}

}
