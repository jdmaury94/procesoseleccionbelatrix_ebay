package com.belatrix.xpageobjects;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.openqa.selenium.WebElement;

import com.belatrix.operaciones.Operaciones;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ResultsPageObject extends PageObject {

	Operaciones operaciones;
	
	@FindBy(xpath="//h1//span[@class='rcnt']")
	WebElementFacade resultsNumber;
	
	@FindBy(id="DashSortByContainer")//w4-w1
	WebElementFacade lstSortOptions;
	
	@FindBy(xpath="//li//a[contains(text(),'Precio + Envío: más bajo primero')]")
	WebElementFacade metodoOrden;
	
	public ResultsPageObject(){
		operaciones=new Operaciones();
	}
	
	public void brandFilter(String brand) {
		//lblPuma.click();
		String xpathMarca="//input[@aria-label='"+brand+"']";
		getDriver().findElement(By.xpath(xpathMarca)).click();
	}
	
	public void sizeFilter(String size) {
		String xpathSize="//input[@aria-label='"+size+"']";
		getDriver().findElement(By.xpath(xpathSize)).click();
	}
	
	public void printNumberOfResults() {
		String number,locator="";
		try {
			locator="//h1[@class='srp-controls__count-heading']";
			number=getDriver().findElement(By.xpath(locator)).getText();
		}
		catch(Exception ex) {
			locator="//h1//span[@class='rcnt']";
			number=getDriver().findElement(By.xpath(locator)).getText();
		}
		System.out.println("************************************************ EJERCICIO 5 TALLER ************************************************");
		System.out.println("Numero de resultados es......... "+number+" (Antes de hacer filtro por precio más bajo)");
		System.out.println("********************************************************************************************************************\n");
	}
	
	public void sortByPriceAsc() {
		String locator="DashSortByContainer";
		//WebElementFacade we;
		try {
			getDriver().findElement(By.id(locator)).click();
			metodoOrden.click();
			
		}catch(Exception ex) {
			locator="//label[@for='w4-w1_btn']"; //Ordenar por
			getDriver().findElement(By.xpath(locator)).click();
			locator="//span[contains(text(),'Precio + Envío: más bajo primero')]";
			getDriver().findElement(By.xpath(locator)).click();
		}
	}
	
	public void traerPrecios() {
		operaciones=new Operaciones();
		String xpath="(//span[@class=' bold'])[position()<6]";
		List<WebElement> elements=getDriver().findElements(By.xpath(xpath));
		List<Double>listaPrecios=new ArrayList<Double>();
		
		if(elements.size()==0) {
			xpath="(//span[@class='s-item__price'])[position()<6]";
			elements=getDriver().findElements((By.xpath(xpath)));
		}
		//elements=elements.subList(0, 5);
		for(WebElement e:elements) {
			listaPrecios.add(operaciones.stringADouble(e.getText()));
		}
		assertFalse(operaciones.checkListSorted(listaPrecios));//La lista no está ordenada
	}
	
	public void imprimirProductoYPrecioPorConsola() {
		
		System.out.println("************************************************ EJERCICIO 8 TALLER ************************************************");
		
		String nombreProductoXpath="(//h3/a[@class='vip'])[position()=";		
		String precioProductoXpath="(//li[@class='sresult gvresult ']//span[@class=' bold'])[position()=";
		String primeros5ProductosXpath="(//li[@class='sresult gvresult '])[position()<6]";
		
		List<WebElement> primeros5Productos=getDriver().findElements(By.xpath(primeros5ProductosXpath));
		if(primeros5Productos.size()==0) {
			primeros5ProductosXpath="(//div[@class='s-item__wrapper clearfix'])[position()<6]";
			primeros5Productos=getDriver().findElements(By.xpath(primeros5ProductosXpath));		
			precioProductoXpath="(//div[@class='s-item__detail s-item__detail--primary'][1]//span[@class='s-item__price'])[position()=";
			nombreProductoXpath="(//h3[@class='s-item__title'])[position()=";
		}
		operaciones=new Operaciones();
		operaciones.imprimirProductoYPrecioPorConsola(primeros5Productos, getDriver(), nombreProductoXpath, precioProductoXpath);
		
		System.out.println("********************************************************************************************************************\n");
	}
	
	public void ordenarImprimirNombreAscendenteOImprimirProductosPrecioDescendente(int accion) {
		String ejercicio="";
		if(accion==1)
			ejercicio="9";
		else
			ejercicio="10";
		
		System.out.println("************************************************ EJERCICIO "+ejercicio+" TALLER ************************************************");
		
		String xpathProductos="//li[@class='sresult gvresult ']";
		String xpathNombreProducto="(//li[@class='sresult gvresult ']//h3)[position()=";
		String xpathPrecioProducto="(//span[contains(@class,'bold')])[position()=";
		List<WebElement> listaElementos=new ArrayList<>();
		
			listaElementos=getDriver().findElements(By.xpath(xpathProductos));
			if(listaElementos.size()==0) {
				xpathProductos="//div[@class='s-item__wrapper clearfix']";
				listaElementos=getDriver().findElements(By.xpath(xpathProductos));	
				xpathNombreProducto="(//h3[@class='s-item__title'])[position()=";
				xpathPrecioProducto="(//div[@class='s-item__detail s-item__detail--primary'][1]//span[@class='s-item__price'])[position()=";			
			}
		operaciones=new Operaciones();
		if(accion==1)
			operaciones.ordenarEImprimirProductosPorNombreAscendentemente(listaElementos, getDriver(),xpathNombreProducto,xpathPrecioProducto);
		else
			operaciones.ordenarEImprimirProductosPorPrecioDescendentemente(listaElementos, getDriver(),xpathNombreProducto,xpathPrecioProducto);
		System.out.println("*********************************************************************************************************************\n");
		
	}
	
	public void ordenarEImprimirProductosPorPrecioDescendentemente() {
		
		/*System.out.println("************************************************ EJERCICIO 10 TALLER ************************************************");
		
		String xpathProductos="//li[@class='sresult gvresult ']";
		String xpathNombreProducto="(//li[@class='sresult gvresult ']//h3)[position()=";
		String xpathPrecioProducto="(//span[contains(@class,'bold')])[position()=";
		List<WebElement> listaElementos=new ArrayList<>();
		
		listaElementos=getDriver().findElements(By.xpath(xpathProductos));
		if(listaElementos.size()==0) {
			xpathProductos="//div[@class='s-item__wrapper clearfix']";
			listaElementos=getDriver().findElements(By.xpath(xpathProductos));	
			xpathNombreProducto="(//h3[@class='s-item__title'])[position()=";
			xpathPrecioProducto="(//div[@class='s-item__detail s-item__detail--primary'][1]//span[@class='s-item__price'])[position()=";			
		}
		
		operaciones=new Operaciones();
		operaciones.ordenarEImprimirProductosPorPrecioDescendentemente(listaElementos, getDriver(),xpathNombreProducto,xpathPrecioProducto);
		
		System.out.println("********************************************************************************************************************\n");*/
		
	}
}
