package com.belatrix.ebay.definitions;

import com.belatrix.ebay.steps.ResultsSteps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class ResultsDefinition {
	
	@Steps
	ResultsSteps resultsSteps;
	
	@And("^filtro por la marca \"([^\"]*)\"$")
	public void filtro_por_la_marca(String marca)  {
	    resultsSteps.filtro_por_la_marca(marca);
	}

	@And("^selecciono la talla \"([^\"]*)\"$")
	public void selecciono_la_talla(String numeroTalla)  {
	  resultsSteps.selecciono_la_talla(numeroTalla);  
	}
	
	@Then("^imprimo el numero de resultados por consola$")
	public void imprimo_el_numero_de_resultados_por_consola()  {
	    resultsSteps.imprimo_el_numero_de_resultados_por_consola();
	}
	

	@And("^ordeno resultados ascendentemente$")
	public void ordeno_resultados_ascendentemente()  {
		resultsSteps.ordeno_resultados_ascendentemente();
	}
	

	@Then("^verifico el orden con los primeros \"([^\"]*)\" resultados$")
	public void verifico_el_orden_con_los_primeros_resultados(String numeroResultados)  {
	    resultsSteps.verifico_el_orden_con_los_primeros_resultados(numeroResultados);
	}
	
	@And("^imprimo por consola los primeros \"([^\"]*)\" productos con sus precios$")
	public void imprimo_por_consola_los_primeros_productos_con_sus_precios(String numeroProductos)  {
		resultsSteps.imprimo_por_consola_los_primeros_productos_con_sus_precios(numeroProductos);
	}
	

	@And("^ordeno e imprimo los productos por nombre ascendentemente$")
	public void ordeno_e_imprimo_los_productos_por_nombre_ascendentemente()  {
	    resultsSteps.ordeno_e_imprimo_los_productos_por_nombre_ascendentemente();
	}


	@And("^ordeno e imprimo los productos por precio descendentemente$")
	public void ordeno_e_imprimo_los_productos_por_precio_descendentemente()  {
	    resultsSteps.ordeno_e_imprimo_los_productos_por_precio_descendentemente();
	}


}
