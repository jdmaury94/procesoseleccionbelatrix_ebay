package com.belatrix.ebay.definitions;

import com.belatrix.ebay.steps.HomeSteps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class HomeDefinition {
	
	@Steps
	HomeSteps homeSteps;
	
	@Given("^ingreso a la pagina \"([^\"]*)\"$")
	public void ingreso_a_la_pagina(String url)  {
	    homeSteps.ingreso_a_la_pagina(url);
	}

	@And("^ingreso \"([^\"]*)\" en el buscador$")
	public void ingreso_en_el_buscador(String palabraBusqueda)  {
		homeSteps.ingreso_en_el_buscador(palabraBusqueda);
	}

}
