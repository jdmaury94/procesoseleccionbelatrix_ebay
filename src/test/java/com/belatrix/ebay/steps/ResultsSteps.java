package com.belatrix.ebay.steps;

import com.belatrix.xpageobjects.ResultsPageObject;

import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Step;

public class ResultsSteps {
	
	ResultsPageObject resultsPageObject;
	
	@Step
	public void filtro_por_la_marca(String marca) {
		resultsPageObject.brandFilter(marca);
	}
	
	@Step
	public void selecciono_la_talla(String size)  {
	    resultsPageObject.sizeFilter(size);
	}
	
	@Step
	public void imprimo_el_numero_de_resultados_por_consola()  {
	    resultsPageObject.printNumberOfResults();
	}
	
	@Step
	public void ordeno_resultados_ascendentemente()  {
	    resultsPageObject.sortByPriceAsc();
	}
	
	@Step
	public void verifico_el_orden_con_los_primeros_resultados(String numeroResultados)  {
		resultsPageObject.traerPrecios();   
	}
	
	@Step
	public void imprimo_por_consola_los_primeros_productos_con_sus_precios(String numeroProductos)  {
		resultsPageObject.imprimirProductoYPrecioPorConsola();
	}

	@Step
	public void ordeno_e_imprimo_los_productos_por_nombre_ascendentemente()  {
		resultsPageObject.ordenarImprimirNombreAscendenteOImprimirProductosPrecioDescendente(1);
	}
	
	@Step
	public void ordeno_e_imprimo_los_productos_por_precio_descendentemente()  {
	    resultsPageObject.ordenarImprimirNombreAscendenteOImprimirProductosPrecioDescendente(2);
	}


}
