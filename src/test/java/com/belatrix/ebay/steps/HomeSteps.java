package com.belatrix.ebay.steps;

import com.belatrix.xpageobjects.HomePageObject;

import net.thucydides.core.annotations.Step;

public class HomeSteps {
	
	HomePageObject homePageObject;
	
	@Step
	public void ingreso_a_la_pagina(String url) {
		homePageObject.openAt(url);
	}
	
	@Step
	public void ingreso_en_el_buscador(String palabraBusqueda)  {
	    homePageObject.inputTextSalesPerson(palabraBusqueda);
	    homePageObject.clickSearchBtn();
	}

}
