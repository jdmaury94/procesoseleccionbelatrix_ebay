package com.belatrix.ebay;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/features/busquedaProducto.feature", tags = "@busquedaPuma")
public class RunnerTags {

}
