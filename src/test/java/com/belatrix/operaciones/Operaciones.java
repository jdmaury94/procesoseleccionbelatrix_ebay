package com.belatrix.operaciones;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.thucydides.core.annotations.findby.By;
public class Operaciones {
	
	public double stringADouble(String cadenaPrecioProducto) {
		double valorProducto=0.0;
		String precioFormateado=cadenaPrecioProducto.replace("COP $", "").replace(" ", "");
		try {
			valorProducto=Double.parseDouble(precioFormateado);
		}catch(Exception ex) {
			int letraA=precioFormateado.indexOf("a");
			valorProducto=Double.parseDouble(precioFormateado.substring(0,letraA));
		}
		return valorProducto;		
	}
	
	public boolean checkListSorted(List<Double>listaPrecios) {
		for(int i=0;i<listaPrecios.size()-1;i++){
			if(listaPrecios.get(i)>listaPrecios.get(i+1))
				return false;
		}
		return true;
	}
	
	
	public void imprimirProductoYPrecioPorConsola(List<WebElement> lista5Elementos,WebDriver getDriver,String xpathNombreProducto,String xpathPrecioProducto) {
		int item=1;
		for(WebElement e:lista5Elementos) {
			System.out.println("Nombre del producto: "+getDriver.findElement(By.xpath(xpathNombreProducto+item+"]")).getText());
			System.out.println("Precio del producto: "+getDriver.findElement(By.xpath(xpathPrecioProducto+item+"]")).getText());
			item++;
		}
	}
	
	public void ordenarEImprimirProductosPorNombreAscendentemente(List<WebElement> listaElementos,WebDriver getDriver,String xpathNombreProducto,String xpathPrecioProducto) {
		Map<String,Double> mapaProductos=new TreeMap<String,Double>();
		String nombreProducto="";
		Double precioProducto=0.0; 
		int item=1;
		System.out.println("Numero total de elementos es .......... "+listaElementos.size());
		for(WebElement elemento:listaElementos) {
			nombreProducto=getDriver.findElement(By.xpath(xpathNombreProducto+item+"]")).getText();
			precioProducto=stringADouble(getDriver.findElement(By.xpath(xpathPrecioProducto+item+"]")).getText());
			mapaProductos.put(nombreProducto, precioProducto);
			item++;
		}
		mapaProductos.forEach((k,v)->System.out.println("Producto : " + k + " Valor : " + v));
	}
	
	public void ordenarEImprimirProductosPorPrecioDescendentemente(List<WebElement> listaElementos,WebDriver getDriver,String xpathNombreProducto,String xpathPrecioProducto) {
		NavigableMap<Double,String> mapaProductos=new TreeMap<Double,String>();		
		String nombreProducto="";
		Double precioProducto=0.0; 
		int item=1;
		for(WebElement elemento:listaElementos) {
			nombreProducto=getDriver.findElement(By.xpath(xpathNombreProducto+item+"]")).getText();
			precioProducto=stringADouble(getDriver.findElement(By.xpath(xpathPrecioProducto+item+"]")).getText());
			mapaProductos.put(precioProducto, nombreProducto);
			item++;
		}
		mapaProductos.descendingMap().forEach((k,v)->System.out.println("Valor : " + k + " Producto : " + v));
	}
	
	

}
