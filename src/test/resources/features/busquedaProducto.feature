@busqueda
Feature: Como automatizador requiero automatizar la busqueda y filtro de producto en ebay para agilizar las pruebas

  @busquedaPuma
  Scenario: Busqueda de producto en la tienda
  	Given ingreso a la pagina "https://www.ebay.com/"
  	And ingreso "shoes" en el buscador
  	And filtro por la marca "PUMA"
  	And selecciono la talla "10"
  	Then imprimo el numero de resultados por consola
  	And ordeno resultados ascendentemente
  	Then verifico el orden con los primeros "5" resultados
  	And imprimo por consola los primeros "5" productos con sus precios
  	And ordeno e imprimo los productos por nombre ascendentemente
  	And ordeno e imprimo los productos por precio descendentemente
  	
  	
		

